//
// Created by repptilia on 18.04.15.
//

#ifndef GOLDENSUN3_PLAYER_H
#define GOLDENSUN3_PLAYER_H

enum Direction {
    LEFT,
    RIGHT,
    UP,
    DOWN,
    UP_LEFT,
    UP_RIGHT,
    DOWN_LEFT,
    DOWN_RIGHT
};

enum Action {
    WALKING,
    RUNNING,
    JUMPING
};

class Player {

protected:
    double m_pos_x;
    double m_pos_y;
    Direction m_dir;
    Action m_action;
};


#endif //GOLDENSUN3_PLAYER_H
