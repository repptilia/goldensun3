//
// Created by repptilia on 18.04.15.
//

#ifndef GOLDENSUN3_CHARACTER_H
#define GOLDENSUN3_CHARACTER_H

#include "Inventory.h"

class Character {
public:
    Character();

protected:
    Inventory m_inventory;

};


#endif //GOLDENSUN3_CHARACTER_H
