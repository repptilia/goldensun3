//
// Created by repptilia on 18.04.15.
//

#ifndef GOLDENSUN3_ITEM_H
#define GOLDENSUN3_ITEM_H


#include <string>
#include <vector>
#include "ItemStat.h"

class Item {
public:
    Item(std::string &name, unsigned int const id, unsigned int const quantity, bool stackable,
         std::vector<ItemStat> const &stats) : name(name), id(id), quantity(quantity), stackable(stackable),
                                               stats(stats) { }

    unsigned int const getId() const {
        return id;
    }

    std::vector<ItemStat> const &getStats() const {
        return stats;
    }

    std::string &getName() const {
        return name;
    }
    bool isStackable() const {
        return stackable;
    }

    unsigned int getQuantity() const {
        return quantity;
    }

    void setQuantity(unsigned int quantity) {
        Item::quantity = quantity;
    }

    const unsigned int changeQuantityBy(const int quantity){
        Item::quantity += quantity;

        if (quantity < 0)
            Item::quantity = 0;

        return Item::quantity;
    }

protected:
    std::string name;
    std::vector<ItemStat> stats;
    const unsigned int id;
    unsigned int quantity;
    bool stackable;
};


#endif //GOLDENSUN3_ITEM_H
