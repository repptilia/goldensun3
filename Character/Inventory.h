//
// Created by repptilia on 18.04.15.
//

#ifndef GOLDENSUN3_INVENTORY_H
#define GOLDENSUN3_INVENTORY_H

#include <vector>
#include "Item.h"

class Inventory {
public:
    Inventory();

    bool add(Item item);
    bool add(Item item, const int quantity);

protected:
    std::vector<Item> items;

};


#endif //GOLDENSUN3_INVENTORY_H
