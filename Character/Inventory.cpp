//
// Created by repptilia on 18.04.15.
//

#include "Inventory.h"

Inventory::Inventory() { }

bool Inventory::add(Item item) {
    return add(item,item.getQuantity());
}

bool Inventory::add(Item item, const int quantity) {
    // If item can't be stacked, add it
    if (!item.isStackable()) {
        item.setQuantity(quantity);
        items.push_back(item);
        return true;
    }

    // Otherwise, check for existing item
    for (std::vector<Item>::iterator i = items.begin(); i != items.end(); ++i)
    {
        // Check that no MultipleItem already has the item
        if ((*i).getId() == item.getId()) {
            (*i).changeQuantityBy(quantity);

            return true;
        }
    }

    // Finally, add item
    items.push_back(item);
}